///
/// font reducer
///
// parameters of the font
var default_size = 42;
var min_size = 33;
// ingorred blocks in height calculations
var ignorred_blocks = ["flag_container", "description"];
// get sides off all the cards
var sides = document.getElementsByClassName("card_side");
// only even sides are needed (front)
for(var index = 0; index < sides.length; ++index)
{
    var side = sides[index];
    // reducing size of the main_content because of the header
    var header = side.children[0];
    var content = side.querySelector(".main_content");
    var content_margin = parseFloat(getComputedStyle(content, null).getPropertyValue("margin-bottom")).toFixed(0)       // issues with counting border width not counted

    if((index % 2) != 0)        // reduces .main_content on the back side
    {
        content.style.height = side.clientHeight - header.clientHeight + "px";
        continue;
    }
    else if(side.clientHeight > content.clientHeight + header.clientHeight) // do we need to reduce the font?
        continue;   // elements are not exceeding the card side

    // reducing font size of the Description
    var occupied_height = 0;
    for(var child = 0; child < content.childElementCount; ++child)
    {
        // in case our child is in the black list
        if(ignorred_blocks.indexOf(content.children[child].className) != -1)
            continue;

        occupied_height += content.children[child].clientHeight;
    }

    // target hight it tries to reach
    var target_height = content.clientHeight - occupied_height - content_margin;        // fix: border width is missing (content_margin)

    var description_section = content.querySelector('.description');
    var description = description_section.lastElementChild;

    for(var size = default_size; description_section.clientHeight > target_height; --size)
    {
        description.style.fontSize = size + "px";
        if(size < min_size)
        {
            var card_index = index / 2;
            console.log("font for description becomes too small! Card:", card_index.toFixed(0));
            description.style.backgroundColor = "red";
            break;
        }
    }
}